<?php
	$page = substr($_SERVER['SCRIPT_NAME'], strrpos($_SERVER['SCRIPT_NAME'],'/') + 1);
	$page = str_replace('.php', '', $page);
	$lang = (isset($_GET['lang']) && $_GET['lang'] == 'en') ? 'en' : 'fr';
	function lang($eng, $frn) {
		echo ($GLOBALS['lang'] == 'en') ? $eng : $frn;
	}
?>
<!DOCTYPE html>
<html lang="<?= $lang; ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>NH Consulting</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css?v=<?= time(); ?>">
</head>
<body>
<div class="lan">
	<a class="lnk" href="?lang=fr">FR</a> /
	<a class="lnk" href="?lang=en">EN</a>
</div>