<?php 
  include 'header.php';
  include 'banner.php';
?>

<div class="container py-5">

<div id="exam">
  <a class="active" onclick="tabChange(this, '#exam a', '#tab1, #tab4', '#target div, #target2 div')">1</a>
  <a onclick="tabChange(this, '#exam a', '#tab2, #tab5', '#target div, #target2 div')">1</a>
  <a onclick="tabChange(this, '#exam a', '#tab3, #tab6', '#target div, #target2 div')">1</a>
</div>
<div id="target">
  <div id="tab1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia ut maxime eligendi neque iure asperiores,</div>
  <div id="tab2" style="display:none">Voluptas doloremque ratione quae? Nostrum blanditiis perspiciatis corrupti repudiandae maxime.</div>
  <div id="tab3" style="display:none">Incidunt similique adipisci blanditiis itaque.</div>
</div>
<div id="target2">
  <div id="tab4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia ut maxime eligendi neque iure asperiores,</div>
  <div id="tab5" style="display:none">Voluptas doloremque ratione quae? Nostrum blanditiis perspiciatis corrupti repudiandae maxime.</div>
  <div id="tab6" style="display:none">Incidunt similique adipisci blanditiis itaque.</div>
</div>

</div>

<?php include 'footer.php'; ?>