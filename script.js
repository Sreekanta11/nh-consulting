$(document).ready(function () {
	$("form").on("change", ".file-upload-field", function () {
		$(this).parent(".file-upload-wrapper").attr("data-text",
			$(this).val().replace(/.*(\/|\\)/, ''));
	});
});

function tabChange(ele, ownSiblings, target, targetSiblings) {
	console.log(target);
	if ($(ele).hasClass("active")) return;
	$(ownSiblings).removeClass("active");
	$(ele).addClass("active");
	$(targetSiblings).slideUp(500);
	$(target).slideDown(500);
}